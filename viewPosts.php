<?php
// Project Name: Milestone3
// Project Version: 1.2
// Module Name: Blog Post
// Module Version: 1.2
// Programmer Name: Justin Gewecke
// Date: 7/12/2020
// Description: This module handles the blog portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once('myfuncs.php');

$link = dbConnect();

$sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT FROM posts";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);



if ($numRows >= 1)
{
    foreach($result as $i) {
        $sql = "SELECT ID, AUTHOR_ID, TITLE, TEXT FROM posts"; 
        
        $text = $i["TEXT"];
        $title = $i["TITLE"];
        $authorID = $i["AUTHOR_ID"];

        // Get First and Last name of Author from AUTHOR_ID
        $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$authorID'";
        $i = mysqli_query($link, $sql);
        $row = $i->fetch_assoc();	// Read the Row from the Query
        $author = $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"];
        
        // Display result
        echo nl2br("<h1>$title</h1> <h3>by $author</h3> <p>$text</p>");
    }
}

// Close connection
mysqli_close($link);
?>

<a href="index.html">Return to Main Menu.</a>