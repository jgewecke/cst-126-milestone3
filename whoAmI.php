<?php
// Project Name: Milestone3
// Project Version: 1.2
// Module Name: Blog Post
// Module Version: 1.2
// Programmer Name: Justin Gewecke
// Date: 7/12/2020
// Description: This module handles the blog portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once './myfuncs.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Who Am I</title>
</head>

<body>
 <h2>Hello My User ID Is: <?php echo " " . getUserId(); ?></h2><br>
 <a href="index.html">Go to main menu.</a>
</body>

</html>