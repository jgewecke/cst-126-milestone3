<?php
// Project Name: Milestone3
// Project Version: 1.2
// Module Name: Blog Post
// Module Version: 1.2
// Programmer Name: Justin Gewecke
// Date: 7/12/2020
// Description: This module handles the blog portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

function dbConnect() {
    // Connect to azure
    $link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");
    
    // Connect to local
    //$link = mysqli_connect("127.0.0.1", root, root, "activity1");
    
    // Check connection
    if($link === false){
        die("ERROR (myfuncs.php): Could not connect. " . mysqli_connect_error());
    }
    return $link;
}

function saveUserId($id)
{
    session_start();
    $_SESSION["USER_ID"] = $id;
}
function getUserId()
{
    session_start();
    return $_SESSION["USER_ID"];
}


?>