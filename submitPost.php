<!-- 
// Project Name: Milestone3
// Project Version: 1.2
// Module Name: Blog Post
// Module Version: 1.2
// Programmer Name: Justin Gewecke
// Date: 7/12/2020
// Description: This module handles the blog portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp 

--NOTE--
Post title is <=100 characters
Post text is <= 2000 characters
-->

<div class="group">
	<a href="post.php">Create another post.</a>
</div>
<div class="group">
	<a href="viewPosts.php">View All Posts.</a>
</div>


<?php
require_once('myfuncs.php');

$link = dbConnect();

// Simple filter
$bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];

$approved = true;
$titleSizeLimit = 100; // Number of characters allowed
$textSizeLimit = 2000; // Number of characters allowed

// Input
$title = $_POST['Title'];
$text = $_POST['TextInput'];
$author = getUserId();

// Check for bad words
foreach ($bannedWords as $badWord) {
    if (strpos($text, $badWord) !== false) {
        echo("The word '" . $badWord . "' cannot be used. Please remove it and try again");
        $approved = false;
    }
}

// Check for empty fields
if ($text == NULL)  { echo "The text field cannot be blank.\n"; $approved = false; }
if ($title == NULL)  { echo "The title cannot be blank.\n"; $approved = false; }

// Check for appropriate lengths
if (strlen($title) > $titleSizeLimit) {
    echo "Your post title is too big. Please make it 100 characters or less";
    $approved = false;
}
if (strlen($text) > $textSizeLimit) {
    echo "Your blog post is too big. Please make it 2000 characters or less";
    $approved = false;
}

// Is the post approved?
if ($approved) {
    echo "Your post was approved! ";
}
else {
    echo "Your post was not approved.";
    exit;
}

// Attempt insert
$sql = "INSERT INTO posts (AUTHOR_ID, TITLE, TEXT) VALUES ('$author', '$title', '$text')";
if(mysqli_query($link, $sql)){
    echo "Records inserted successfully.";
} else{
    $message = "ERROR: Could not able to execute $sql. " . mysqli_error($link);
    include('./loginFailed.php');
}
?>